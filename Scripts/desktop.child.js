/**
 * YANGLI
 * 子页面操作
 * 2020-10-21 23:46:42
 */
window.Desktop = parent.Desktop; //获取父级Desktop对象的句柄
window.Children = {
	close: function() {
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
		Desktop_parent._closeWin(index);
	},
	newMsg: function(title, content, handle_click) {
		Desktop_parent.newMsg(title, content, handle_click)
	},
	openUrl: function(url, title, max) {
		var click_lock_name = Math.random();
		Desktop_parent._iframe_children[click_lock_name] = true;
		var index = Desktop_parent.openUrl(url, title, max);
		setTimeout(function() {
			delete Desktop_parent._iframe_children[click_lock_name];
		}, 1000)
	}
};
