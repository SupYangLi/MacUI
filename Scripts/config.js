window.Config = {
	Desktop: {
		shortcutClick: "dblclick",
		freeTime: {
			enable: true,
			overTime: 60,
			openIndex: 0
		},
		bideo: {
			enable: true,
			path: [{
				src: './Video/Background/Bird.mov',
				type: 'video/mp4'
			}]
		},
		live2D: {
			enable: true,
			widget: "koharu"
		}
	}
}
