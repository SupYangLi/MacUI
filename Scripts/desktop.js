/**
 *	YANGLI
 * 	桌面设置相关
 * 	2020-10-20 11:28:18
 **/
window.Desktop = {
	_version: 'v1.0',
	_debug: true,
	_bgs: {
		main: '',
		mobile: '',
	},
	_countTask: 0,
	_newMsgCount: 0,
	_animated_classes: [],
	_animated_liveness: 0,
	_switchMenuTooHurry: false,
	_lang: "",
	_iframeOnClick: {
		resolution: 200,
		iframes: [],
		interval: null,
		Iframe: function() {
			this.element = arguments[0];
			this.cb = arguments[1];
			this.hasTracked = false;
		},
		track: function(element, cb) {
			this.iframes.push(new this.Iframe(element, cb));
			if (!this.interval) {
				var _this = this;
				this.interval = setInterval(function() {
					_this.checkClick();
				}, this.resolution);
			}
		},
		checkClick: function() {
			if (document.activeElement) {
				var activeElement = document.activeElement;
				for (var i in this.iframes) {
					var eid = undefined;
					if ((eid = this.iframes[i].element.id) && !document.getElementById(eid)) {
						delete this.iframes[i];
						continue;
					}
					if (activeElement === this.iframes[i].element) { // user is in this Iframe
						if (this.iframes[i].hasTracked === false) {
							this.iframes[i].cb.apply(window, []);
							this.iframes[i].hasTracked = true;
						}
					} else {
						this.iframes[i].hasTracked = false;
					}
				}
			}
		}
	},
	_iframe_children: {},
	_renderBar: function() {
		//调整任务栏项目的宽度
		if (this._countTask <= 0) {
			return;
		} //防止除以0
		var btns = $("#task_bar_middle>.btn");
		btns.css('width', ('calc(' + (1 / this._countTask * 100) + '% - 1px )'))
	},
	_handleReady: [],
	_hideShortcut: function() {
		var that = $("#desktop #desktop-shortcuts .shortcut");
		that.removeClass('animated flipInX');
		that.addClass('animated flipOutX');
	},
	_showShortcut: function() {
		var that = $("#desktop #desktop-shortcuts .shortcut");
		that.removeClass('animated flipOutX');
		that.addClass('animated flipInX');
	},
	_checkBgUrls: function() {
		var loaders = $('#desktop>.img-loader');
		var flag = false;
		if (Desktop.isSmallScreen()) {
			if (Desktop._bgs.mobile) {
				loaders.each(function() {
					var loader = $(this);
					if (loader.attr('src') === Desktop._bgs.mobile && loader.hasClass('loaded')) {
						Desktop._setBackgroundImg(Desktop._bgs.mobile);
						flag = true;
					}
				});
				if (!flag) {
					//没找到加载完毕的图片
					var img = $('<img class="img-loader" src="' + Desktop._bgs.mobile + '" />');
					$('#desktop').append(img);
					Desktop._onImgComplete(img[0], function() {
						img.addClass('loaded');
						Desktop._setBackgroundImg(Desktop._bgs.mobile);
					})
				}
			}
		} else {
			if (Desktop._bgs.main) {
				loaders.each(function() {
					var loader = $(this);
					if (loader.attr('src') === Desktop._bgs.main && loader.hasClass('loaded')) {
						Desktop._setBackgroundImg(Desktop._bgs.main);
						flag = true;
					}
				});
				if (!flag) {
					//没找到加载完毕的图片
					var img = $('<img class="img-loader" src="' + Desktop._bgs.main + '" />');
					$('#desktop').append(img);
					Desktop._onImgComplete(img[0], function() {
						img.addClass('loaded');
						Desktop._setBackgroundImg(Desktop._bgs.main);
					})
				}
			}
		}
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	动画设置
	 * */
	_startAnimate: function() {
		setInterval(function() {
			var classes_lenth = Desktop._animated_classes.length;
			var animated_liveness = Desktop._animated_liveness;
			if (animated_liveness === 0 || classes_lenth === 0 || !$("#desktop-menu").hasClass('opened')) {
				return;
			}
			$('#desktop-menu>.blocks>.menu_group>.block').each(function() {
				if (!$(this).hasClass('onAnimate') && Math.random() <= animated_liveness) {
					var that = $(this);
					var class_animate = Desktop._animated_classes[Math.floor((Math.random() * classes_lenth))];
					that.addClass('onAnimate');
					setTimeout(function() {
						that.addClass(class_animate);
						setTimeout(function() {
							that.removeClass('onAnimate');
							that.removeClass(class_animate);
						}, 3000);
					}, Math.random() * 2 * 1000)
				}
			})
		}, 1000);
	},
	/**
	 *	YANGLI
	 * 	图片加载完成
	 * 	2020-10-20 11:30:13
	 **/
	_onImgComplete: function(img, callback) {
		if (!img) {
			return;
		}
		var timer = setInterval(function() {
			if (img.complete) {
				callback(img);
				clearInterval(timer);
			}
		}, 50)
	},
	/**
	 *	YANGLI
	 * 	设置背景图片
	 * 	2020-10-20 11:30:13
	 **/
	_setBackgroundImg: function(img) {
		$('#desktop').css('background-image', 'url(' + img + ')')
	},
	_settop: function(layero) {
		if (!isNaN(layero)) {
			layero = this.getLayeroByIndex(layero);
		}
		//置顶窗口
		var max_zindex = 0;
		$(".desktop-open-iframe").each(function() {
			z = parseInt($(this).css('z-index'));
			$(this).css('z-index', z - 1);
			if (z > max_zindex) {
				max_zindex = z;
			}
		});
		layero.css('z-index', max_zindex + 1);
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	置顶检查
	 * */
	_checkTop: function() {
		var max_index = 0,
			max_z = 0,
			btn = null;
		$("#task_bar_middle .btn.show").each(function() {
			var index = $(this).attr('index');
			var layero = Desktop.getLayeroByIndex(index);
			var z = layero.css('z-index');
			if (z > max_z) {
				max_index = index;
				max_z = z;
				btn = $(this);
			}
		});
		this._settop(max_index);
		$("#task_bar_middle .btn").removeClass('active');
		if (btn) {
			btn.addClass('active');
		}
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	右键菜单渲染
	 * */
	_renderContextMenu: function(x, y, menu, trigger) {
		this._removeContextMenu();
		if (menu === true) {
			return;
		}
		var dom = $("<div class='desktop-context-menu'><ul></ul></div>");
		$('#desktop').append(dom);
		var ul = dom.find('ul');
		for (var i = 0; i < menu.length; i++) {
			var item = menu[i];
			if (item === '|') {
				ul.append($('<hr/>'));
				continue;
			}
			if (typeof(item) === 'string') {
				ul.append($('<li>' + item + '</li>'));
				continue;
			}
			if (typeof(item) === 'object') {
				var sub = $('<li><i class="' + item.icon + '"></i>' + item.name + '</li>');
				ul.append(sub);
				sub.click(trigger, item.func);
				continue;
			}
		}
		//修正坐标
		if (x + 150 > document.body.clientWidth) {
			x -= 150
		}
		if (y + dom.height() > document.body.clientHeight) {
			y -= dom.height()
		}
		dom.css({
			top: y,
			left: x,
		});
	},
	_removeContextMenu: function() {
		$('.desktop-context-menu').remove();
	},
	/**
	 *	YANGLI
	 * 	关闭弹窗
	 * 	2020-10-20 11:30:13
	 **/
	_closeWin: function(index) {
		$("#desktop_" + index).remove();
		layer.close(index);
		Desktop._checkTop();
		Desktop._countTask--; //回退countTask数
		Desktop._renderBar();
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	桌面图标位置刷新
	 * */
	_reload_shortcus: function() {
		Desktop.renderShortcuts();
		Desktop._checkBgUrls();
		Desktop._fixWindowsHeightAndWidth();
		Desktop.renderDocks();
	},
	/**
	 * 根据模板生成HTML
	 * @param {选择器 或 字符串} e 
	 * @param {Json} o
	 * */
	_template: function(e, o) {
		let source = "";

		if (typeof e == "string") {
			source = e;
		} else {
			source = e.html();
		}

		let template = Handlebars.compile(source, {
			noEscape: true
		});
		return template(o);
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	Iframe 位置修正
	 * */
	_fixWindowsHeightAndWidth: function() {
		//此处代码修正全屏切换引起的子窗体尺寸超出屏幕
		var opens = $('.desktop-open-iframe');
		var clientHeight = document.body.clientHeight;
		opens.each(function() {
			var layero_opened = $(this);
			var height = layero_opened.css('height');
			height = parseInt(height.replace('px', ''));
			if (height + 40 >= clientHeight) {
				layero_opened.css('height', clientHeight - 40);
				layero_opened.find('.layui-layer-content').css('height', clientHeight - 83);
				layero_opened.find('.layui-layer-content iframe').css('height', clientHeight - 83);
			}
		})
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	获取菜单
	 * */
	_load_shortcuts: function() {
		$.ajax({
			url: "./Json/Shortcuts.json",
			type: "get",
			success: function(data) {
				if (!$.isEmptyObject(data)) {
					$("#desktop-shortcuts").append(Desktop._template($("#Template-Shortcut"), {
						menus: data
					}));

					// 重新加载图标位置
					Desktop._reload_shortcus();
				}
			}
		})
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	获取菜单
	 * */
	_load_docks: function() {
		$.ajax({
			url: "./Json/Docks.json",
			type: "get",
			success: function(data) {
				if (!$.isEmptyObject(data)) {
					$("#Docks").append(Desktop._template($("#Template-Dock"), {
						menus: data
					}));
					
					Desktop.renderDocks(); //渲染DOCK
				}
			}
		})
	},

	/**
	 * 桌面图标事件绑定
	 * YANGLI
	 * 2020-10-16 23:03:27
	 */
	_bind_open_windows: function() {
		// 注册事件委派 打开url窗口
		$('#desktop').on(Config.Desktop.shortcutClick, '.shortcut', function() {
			//>> 获取当前点击的对象
			$this = $(this);
			//>> 判断url地址是否为空 如果为空 不予处理
			if ($this.data('url') !== "") {
				//>> 获取弹窗标题
				var title = $this.attr('title') || '',
					areaAndOffset;
				// 获取图标
				var icon = $this.children("i").prop("outerHTML");
				// 标题
				title = icon + title;
				//>> 判断是否需要 设置 区域宽度高度
				if ($this.data('area-offset')) {
					areaAndOffset = $this.data('area-offset');
					//>> 判断是否有分隔符
					if (areaAndOffset.indexOf(',') !== -1) {
						areaAndOffset = eval(areaAndOffset);
					}
				}
				//>> 调用desktop打开url方法
				Desktop.openUrl($this.data('url'), title, areaAndOffset);
			}
		})
	},
	/**
	 * YANGLI
	 * 初始化视频桌面
	 * 2020-10-19 10:13:51
	 * 
	 * */
	_init_bideo: function() {
		var bideo = Config.Desktop.bideo;
		if (bideo.enable) {
			var bv = new Bideo();
			bv.init({
				videoEl: document.querySelector('#background_video'),
				container: document.querySelector('#desktop'),
				resize: true,
				autoplay: true,
				isMobile: false,
				src: bideo.path,
				onLoad: function() {
					//document.querySelector('#video_cover').style.display = 'none';
				}
			});
		}
	},
	/**
	 * 	YANGLI
	 * 	初始化 L2Dwidget
	 * 	2020-10-19 10:15:59
	 * 
	 * */
	_init_live2d: function() {
		var live2D = Config.Desktop.live2D;
		// 看板娘
		if (live2D.enable) {
			if (!(!!window.ActiveXObject || 'ActiveXObject' in window)) {
				L2Dwidget.init({
					dialog: {
						enable: true, // 开启对话框
						script: {
							'every idle 10s': '$hitokoto$', // 每空闲 10 秒钟显示一句话
							'tap face': '哥~~ ❤️', // 当触摸到角色头部时
							'tap body': '哎呀！坏人~', // 当触摸到角色身体时
							'hover #avatar': '🌌 欢迎来到 Abp Admin 桌面管理系统 =^^=', // 当触摸到头像时
							'hover #toggle': '🌌 欢迎来到 Abp Admin 桌面管理系统 =^^='
						}
					},
					display: {
						vOffset: 0
					},
					model: {
						jsonPath: "./Compents/Live2D/Widget/" + live2D.widget + "/assets/" + live2D.widget + ".model.json"
					},
					debug: false,
					log: false
				});
			}
		}
	},
	/**
	 *	YANGLI
	 * 	初始化时间显示
	 * 	2020-10-20 11:30:13
	 **/
	_init_freetime: function() {
		var config = Config.Desktop.freeTime;
		if (config.enable) {
			ifvisible.setIdleDuration(config.overTime);

			// 超过指定时间
			ifvisible.idle(function() {
				console.log(config.overTime)
				layer.open({
					type: 2,
					title: false,
					anim: 1,
					shade: 0.6, //遮罩透明度 shadeClose: false, maxmin: false, //允许全屏最小化 anim: 1, //0-6的动画形式，-1不开启 area:
					area: ['100%', '100%'],
					scrollbar: false,
					closeBtn: false,
					content: "./Html/lock.html"
				});
			});

			// 期间唤醒
			ifvisible.wakeup(function() {
				// 关闭弹窗
				layer.close(config.openIndex);
			});
		}
	},
	/**
	 * 	YANGLI
	 * 	浏览器标签切换事件
	 * 	2020-10-22 21:30:57
	 * 
	 * */
	_init_leave:function(){
		function getHiddenProp() {
			var prefixes = ['webkit', 'moz', 'ms', 'o'];
		
			if ('hidden' in document) return 'hidden';
		
			for (var i = 0; i < prefixes.length; i++) {
				if ((prefixes[i] + 'Hidden') in document)
					return prefixes[i] + 'Hidden';
			}
			return null;
		}
		
		function isHidden() {
			var prop = getHiddenProp();
			if (!prop) return false;
		
			return document[prop];
		}
		
		var visProp = getHiddenProp();
		if (visProp) {
			var evtname = visProp.replace(/[H|h]idden/, '') + 'visibilitychange';
			document.addEventListener(evtname, function() {
				var state = isHidden();
				if (state) {
					document.title = "期待你再次回来哟！"
				} else {
					document.title = "欢迎再次回来！"
				}
			}, false);
		}
	},
	
	/**
	 *	YANGLI
	 * 	初始化
	 * 	2020-10-20 11:30:13
	 **/
	_init: function() {

		Handlebars.registerHelper('IfIconType', function(val, options) {
			if (val.toLowerCase() === "fontawesome") {
				return options.fn(this);
			}
			return options.inverse(this);
		});

		// 初始化视频桌面
		Desktop._init_bideo();
		Desktop._init_live2d();
		//获取语言
		this._lang = (navigator.language || navigator.browserLanguage).toLowerCase();

		// 全屏时间
		Desktop._init_freetime();
		// 菜单加载
		Desktop._load_shortcuts();
		// 加载快捷菜单
		Desktop._load_docks();
		// 图标事件绑定
		Desktop._bind_open_windows();

		$("#desktop_btn_win").click(function() {
			Desktop.commandCenterClose();
			Desktop.menuToggle();
		});
		$("#desktop_btn_command").click(function() {
			Desktop.menuClose();
			Desktop.commandCenterToggle();
		});
		$("#desktop .desktop").click(function() {
			Desktop.menuClose();
			Desktop.commandCenterClose();
		});
		$('#desktop').on('click', ".msg .btn_close_msg", function() {
			var msg = $(this).parent();
			$(msg).addClass('animated slideOutRight');
			setTimeout(function() {
				msg.remove()
			}, 500)
		});
		$('#message_clear').click(function() {
			var msgs = $('#message_center .msg');
			msgs.addClass('animated slideOutRight');
			setTimeout(function() {
				msgs.remove()
			}, 1500);
			setTimeout(function() {
				Desktop.commandCenterClose();
			}, 1000);
		});
		$("#btn-desktop").click(function() {
			$("#desktop .desktop").click();
			Desktop.hideWins();
		});
		$("#desktop-menu-switcher").click(function() {
			if (Desktop._switchMenuTooHurry) {
				return;
			}
			Desktop._switchMenuTooHurry = true;
			var class_name = 'desktop-menu-hidden';
			var list = $("#desktop-menu>.list");
			var blocks = $("#desktop-menu>.blocks");
			var toggleSlide = function(obj) {
				if (obj.hasClass(class_name)) {
					obj.addClass('animated slideInLeft');
					obj.removeClass('animated slideOutLeft');
					obj.removeClass(class_name);
				} else {
					setTimeout(function() {
						obj.addClass(class_name);
					}, 450);
					obj.addClass('animated slideOutLeft');
					obj.removeClass('animated slideInLeft');
				}
			};
			toggleSlide(list);
			toggleSlide(blocks);
			setTimeout(function() {
				Desktop._switchMenuTooHurry = false;
			}, 520)
		});
		$("#task_bar_middle").click(function() {
			$("#desktop .desktop").click();
		});
		$(document).on('click', '.desktop-btn-refresh', function() {
			var index = $(this).attr('index');
			var iframe = Desktop.getLayeroByIndex(index).find('iframe');
			iframe.attr('src', iframe.attr('src'));
		});

		$(document).on('mousedown', '.desktop-open-iframe', function() {
			var layero = $(this);
			Desktop._settop(layero);
			Desktop._checkTop();
		});
		$('#task_bar_middle').on('click', '.btn_close', function() {
			var index = $(this).parent().attr('index');
			Desktop._closeWin(index);
		});
		$('#desktop-menu .list').on('click', '.item', function() {
			var e = $(this);
			if (e.hasClass('has-sub-down')) {
				$('#desktop-menu .list .item.has-sub-up').toggleClass('has-sub-down').toggleClass('has-sub-up');
				$("#desktop-menu .list .sub-item").slideUp();
			}
			e.toggleClass('has-sub-down').toggleClass('has-sub-up');
			while (e.next().hasClass('sub-item')) {
				e.next().slideToggle();
				e = e.next();
			}
		});
		/**	YANGLI
		 * 	2020-10-16 22:58:05
		 * 	浏览器
		 * */
		$("#desktop-btn-browser").click(function() {
			// var area = ['100%', (document.body.clientHeight - 40) + 'px'];
			// var offset = ['0', '0'];
			layer.prompt({
				title: Desktop.lang('访问网址', 'Visit URL'),
				formType: 2,
				value: '',
				skin: 'desktop-layer-open-browser',
				area: ['300px', '150px'],
				zIndex: 99999999999
			}, function(value, i) {
				layer.close(i);
				layer.msg(Language.Dialog.Loading, {
					time: 500
				}, function() {
					Desktop.openUrl(value, value);
				});
			});
		});
		setInterval(function() {
			//重新写mac时间
			var myDate = new Date();
			var week = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")[myDate.getDay()];
			var hour = myDate.getHours();
			var mins = myDate.getMinutes();
			if (mins < 10) {
				mins = '0' + mins
			}
			if (hour >= 0 && hour < 6) {
				hours = '凌晨' + hour;
			} else if (hour >= 6 && hour < 8) {
				hours = '早上' + hour;
			} else if (hour >= 8 && hour < 11) {
				hours = '上午' + hour;
			} else if (hour >= 11 && hour < 13) {
				hours = '中午' + hour;
			} else if (hour >= 13 && hour < 18) {
				hours = '下午' + hour;
			} else {
				hours = '晚上' + hour;
			}
			$("#desktop_btn_time").html(hours + ':' + mins + ' [' + week + ']');
		}, 1000);
		//离开前警告
		// document.body.onbeforeunload = function() {
		// 	window.event.returnValue = Desktop.lang('系统可能不会保存您所做的更改', 'The system may not save the changes you have made.');
		// };
		Desktop.buildList(); //预处理左侧菜单
		Desktop._startAnimate(); //动画处理
		Desktop.renderShortcuts(); //渲染图标
		$("#desktop-shortcuts").removeClass('shortcuts-hidden'); //显示图标
		Desktop._showShortcut(); //显示图标
		Desktop.renderDocks(); //渲染DOCK
		//窗口改大小，重新渲染
		$(window).resize(function() {
			Desktop._reload_shortcus();
		});
		//细节
		$(document).on('focus', ".desktop-layer-open-browser textarea", function() {
			$(this).attr('spellcheck', 'false');
		});
		$(document).on('keyup', ".desktop-layer-open-browser textarea", function(e) {
			if (e.keyCode === 13) {
				$(this).parent().parent().find('.layui-layer-btn0').click();
			}
		});

		//点击清空右键菜单
		$(document).click(function() {
			Desktop._removeContextMenu();
		});
		//禁用右键的右键
		$(document).on('contextmenu', '.desktop-context-menu', function(e) {
			e.preventDefault();
			e.stopPropagation();
		});
		//设置默认右键菜单
		Desktop.setContextMenu('#desktop', true);
		Desktop.setContextMenu('#desktop>.desktop', [{
				icon: "fa fa-fw fa-star",
				name: "收藏本页",
				func: function() {
					
				}
			}, {
				icon: "fa fa-fw fa-window-maximize",
				name: "进入全屏",
				func: function() {
					Desktop.enableFullScreen()
				}
			},
			{
				icon: "fa fa-fw fa-window-restore",
				name: "退出全屏",
				func: function() {
					Desktop.disableFullScreen()
				}
			},
			{
				icon: "fa fa-fw fa-info-circle",
				name: "关于",
				func: function() {
					Desktop.aboutUs()
				}
			}
		]);
		Desktop.setContextMenu('#task_bar_middle', [{
				icon: "fa fa-fw fa-window-maximize",
				name: "全部显示",
				func: function() {
					Desktop.showWins()
				}
			},
			{
				icon: "fa fa-fw fa-window-minimize",
				name: "全部隐藏",
				func: function() {
					Desktop.hideWins()
				}
			},
			{
				icon: "fa fa-fw fa-window-close",
				name: "全部关闭",
				func: function() {
					Desktop.closeAll()
				}
			}
		]);

		//设置消息图标闪烁
		setInterval(function() {
			var btn = $("#desktop-msg-nof.on-new-msg");
			if (btn.length > 0) {
				btn.toggleClass('fa-commenting-o');
			}
		}, 500);

		//绑定快捷键
		$("body").keyup(function(e) {
			if (e.ctrlKey) {
				switch (e.keyCode) {
					case 37: //left
						$("#desktop_btn_win").click();
						break;
					case 38: //up
						Desktop.showWins();
						break;
					case 39: //right
						$("#desktop_btn_command").click();
						break;
					case 40: //down
						Desktop.hideWins();
						break;
				}
			}
		});
		if ($("#desktop-desktop-scene").length < 1) {
			$("#desktop-shortcuts").css({
				position: 'absolute',
				left: 0,
				top: 30,
				'z-index': 100,
			});
			$("#desktop .desktop").append(
				"<div id='desktop-desktop-scene' style='width: 100%;height: calc(100% - 40px);position: absolute;left: 0;top: 0; z-index: 0;background-color: transparent;'></div>"
			)
		}
		
		Desktop._init_leave();
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	设置背景Url
	 * */
	setBgUrl: function(bgs) {
		this._bgs = bgs;
		this._checkBgUrls();
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	菜单关闭
	 * */
	menuClose: function() {
		$("#desktop-menu").removeClass('opened');
		$("#desktop-menu").addClass('hidden');
		this._showShortcut();
		$(".desktop-open-iframe").removeClass('hide');
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	菜单打开
	 * */
	menuOpen: function() {
		$("#desktop-menu").addClass('opened');
		$("#desktop-menu").removeClass('hidden');
		this._hideShortcut();
		$(".desktop-open-iframe").addClass('hide');
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	菜单折叠
	 * */
	menuToggle: function() {
		if (!$("#desktop-menu").hasClass('opened')) {
			this.menuOpen();
		} else {
			this.menuClose();
		}
	},
	/**
	 *	YANGLI
	 * 	消息中心关闭
	 * 	2020-10-20 11:30:13
	 **/
	commandCenterClose: function() {
		$("#message_center").addClass('hidden_right');
		this._showShortcut();
		$(".desktop-open-iframe").removeClass('hide');
	},
	/**
	 *	YANGLI
	 * 	消息中心打开
	 * 	2020-10-20 11:30:13
	 **/
	commandCenterOpen: function() {
		$("#message_center").removeClass('hidden_right');
		this._hideShortcut();
		$(".desktop-open-iframe").addClass('hide');
		$("#desktop-msg-nof").removeClass('on-new-msg fa-commenting-o');
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	渲染快捷桌面图标位置
	 * */
	renderShortcuts: function() {
		var h = parseInt(($("#desktop #desktop-shortcuts")[0].offsetHeight - 60) / 90);
		var x = 0,
			y = 0;
		$("#desktop #desktop-shortcuts .shortcut").each(function() {
			$(this).css({
				left: x * 82 + 10,
				top: y * 90 + 10,
			});
			y++;
			if (y >= h) {
				y = 0;
				x++;
			}
		});
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	渲染快捷菜单位置
	 * */
	renderDocks: function() {
		var cell_width = 74;
		var width = document.body.clientWidth;
		var docks = $("#footer .dock li");
		var max_num = parseInt(width / cell_width) - 1;

		for (var i = 0; i < docks.length; i++) {
			if (i > max_num) {
				docks.eq(i).css('display', 'none');
			} else {
				docks.eq(i).css('display', 'list-item');
			}
		}
	},
	/**
	 *	YANGLI
	 * 	消息中心切换
	 * 	2020-10-20 11:30:13
	 **/
	commandCenterToggle: function() {
		if ($("#message_center").hasClass('hidden_right')) {
			this.commandCenterOpen();
		} else {
			this.commandCenterClose();
		}
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	右侧消息推送
	 * */
	newMsg: function(title, content, handle_click) {
		var e = $('<div class="msg">' +
			'<div class="title">' + title + '</div>' +
			'<div class="content">' + content + '</div>' +
			'<span class="btn_close_msg fa fa-close"></span>' +
			'</div>');
		$("#message_center .msgs").prepend(e);
		e.find('.content:first,.title:first').click(function() {
			if (handle_click) {
				handle_click(e);
			}
		});
		layer.tips(Desktop.lang('新消息:', 'New message:') + title, '#desktop_btn_command', {
			tips: [1, '#3c6a4a'],
			time: 3000
		});
		if ($("#message_center").hasClass('hidden_right')) {
			$("#desktop-msg-nof").addClass('on-new-msg');
		}
	},
	getLayeroByIndex: function(index) {
		return $('#' + 'layui-layer' + index)
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	是否小屏
	 * */
	isSmallScreen: function(size) {
		if (!size) {
			size = 768
		}
		var width = document.body.clientWidth;
		return width < size;
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	启用全屏
	 * */
	enableFullScreen: function() {
		var docElm = document.documentElement;
		//W3C
		if (docElm.requestFullscreen) {
			docElm.requestFullscreen();
		}
		//FireFox
		else if (docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
		}
		//Chrome等
		else if (docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
		}
		//IE11
		else if (docElm.msRequestFullscreen) {
			document.body.msRequestFullscreen();
		}
	},
	/**	YANGLI
	 * 	2020-10-16 22:58:05
	 * 	禁用全屏
	 * */
	disableFullScreen: function() {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	},
	buildList: function() {
		$("#desktop-menu .list .sub-item").slideUp();
		$("#desktop-menu .list .item").each(function() {
			if ($(this).next().hasClass('sub-item')) {
				$(this).addClass('has-sub-down');
				$(this).removeClass('has-sub-up');
			}
		})
	},
	/**
	 *	YANGLI
	 * 	弹窗
	 * 	2020-10-20 11:30:13
	 **/
	openUrl: function(url, title, areaAndOffset) {
		if (this._countTask > 12) {
			layer.msg("您打开的太多了，歇会儿吧~");
			return false;
		} else {
			this._countTask++;
		}
		if (!url) {
			url = '404'
		}
		url = url.replace(/(^\s*)|(\s*$)/g, "");
		var preg = /^(https?:\/\/|\.\.?\/|\/\/?)/;
		if (!preg.test(url)) {
			url = 'http://' + url;
		}
		if (!url) {
			url = '//yuri2.cn';
		}
		if (!title) {
			title = url;
		}
		var area, offset;
		if (this.isSmallScreen() || areaAndOffset === 'max') {
			area = ['100%', (document.body.clientHeight - 30) + 'px'];
			offset = ['30px', '0'];
		} else if (typeof areaAndOffset === 'object') {
			area = areaAndOffset[0];
			offset = areaAndOffset[1];
		} else {
			area = ['80%', '80%'];
			var topset, leftset;
			topset = parseInt($(window).height());
			topset = (topset - (topset * 0.8)) / 2 - 31;
			leftset = parseInt($(window).width());
			leftset = (leftset - (leftset * 0.8)) / 2 - 120;
			offset = [Math.round((this._countTask % 10 * 20) + topset) + 'px', Math.round((this._countTask % 10 * 20 + 100) +
				leftset) + 'px'];
		}
		var index = layer.open({
			type: 2,
			shadeClose: true,
			shade: false,
			maxmin: true, //开启最大化最小化按钮
			title: title,
			content: url,
			area: area,
			resize: true,
			offset: offset,
			isOutAnim: false,
			skin: 'desktop-open-iframe',
			cancel: function(index, layero) {
				$("#desktop_" + index).remove();
				Desktop._checkTop();
				Desktop._countTask--; //回退countTask数
				Desktop._renderBar();
			},
			min: function(layero) {
				layero.hide();
				$("#desktop_" + index).removeClass('show');
				Desktop._checkTop();
				return false;
			},
			full: function(layero) {
				layero.find('.layui-layer-min').css('display', 'inline-block');
				layero_opened.css('margin-top', 30);
			},
		});
		$('#task_bar_middle .btn.active').removeClass('active');
		var btn = $('<div id="desktop_' + index + '" index="' + index + '" class="btn show active"><div class="btn_title">' +
			title + '</div><div class="btn_close fa fa-close"></div></div>');
		var layero_opened = Desktop.getLayeroByIndex(index);
		layero_opened.css('z-index', Desktop._countTask + 813);
		Desktop._settop(layero_opened);
		//重新定义菜单布局
		layero_opened.find('.layui-layer-setwin').prepend('<a class="desktop-btn-refresh" index="' + index +
			'" href="#"></a>');
		layero_opened.find('.layui-layer-setwin .layui-layer-max').click(function() {
			setTimeout(function() {
				var height = layero_opened.css('height');
				height = parseInt(height.replace('px', ''));
				if (height >= document.body.clientHeight) {
					layero_opened.css('height', height - 30);
					layero_opened.find('.layui-layer-content').css('height', height - 72);
					layero_opened.find('.layui-layer-content iframe').css('height', height - 72);
				}
			}, 300);

		});
		$("#task_bar_middle").append(btn);
		Desktop._renderBar();
		btn.click(function() {
			var index = $(this).attr('index');
			var layero = Desktop.getLayeroByIndex(index);
			var settop = function() {
				//置顶窗口
				var max_zindex = 0;
				$(".desktop-open-iframe").each(function() {
					z = parseInt($(this).css('z-index'));
					$(this).css('z-index', z - 1);
					if (z > max_zindex) {
						max_zindex = z;
					}
				});
				layero.css('z-index', max_zindex + 1);
			};
			if ($(this).hasClass('show')) {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).removeClass('show');
					Desktop._checkTop();
					layero.hide();
				} else {
					$('#task_bar_middle .btn.active').removeClass('active');
					$(this).addClass('active');
					Desktop._settop(layero);
				}
			} else {
				$(this).addClass('show');
				$('#task_bar_middle .btn.active').removeClass('active');
				$(this).addClass('active');
				Desktop._settop(layero);
				layero.show();
			}
		});

		Desktop._iframeOnClick.track(layero_opened.find('iframe:first')[0], function() {
			if (Object.getOwnPropertyNames(Desktop._iframe_children).length === 0) {
				Desktop._settop(layero_opened);
				Desktop._checkTop();
			} else {
				console.log('click locked');
			}
		});
		
		this.menuClose();
		this.commandCenterClose();
		return index;
	},
	/**
	 *	YANGLI
	 * 	关闭所有
	 * 	2020-10-20 11:30:13
	 **/
	closeAll: function() {
		$(".desktop-open-iframe").remove();
		$("#task_bar_middle").html("");
		Desktop._countTask = 0;
		Desktop._renderBar();
	},
	/**
	 *	YANGLI
	 * 	设置动画方式
	 * 	2020-10-20 11:30:13
	 **/
	setAnimated: function(animated_classes, animated_liveness) {
		this._animated_classes = animated_classes;
		this._animated_liveness = animated_liveness;
	},
	/**
	 *	YANGLI
	 * 	退出
	 * 	2020-10-20 11:30:13
	 **/
	exit: function() {
		layer.confirm(Desktop.lang('确认要关闭本页吗?', 'Are you sure you want to close this page?'), {
			icon: 3,
			title: Desktop.lang('提示', 'Prompt')
		}, function(index) {
			document.body.onbeforeunload = function() {};
			window.location.href = "about:blank";
			window.close();
			layer.close(index);
			layer.alert(Desktop.lang('哎呀,好像失败了呢。', 'Ops...There seems to be a little problem.'), {
				skin: 'layui-layer-lan',
				closeBtn: 0
			});
		});

	},
	lang: function(cn, en) {
		return this._lang === 'zh-cn' || this._lang === 'zh-tw' ? cn : en;
	},
	aboutUs: function() {
		//关于我们
		layer.open({
			type: 1,
			closeBtn: 1, //不显示关闭按钮
			anim: 2,
			skin: 'desktop-open-iframe',
			title: "关于我们",
			shadeClose: true, //开启遮罩关闭
			area: ['320px', '200px'], //宽高
			content: '<div style="padding: 10px;font-size: 12px">' +
				'<p>欢迎使用仿 MacUI' +
				'<p>YANGLI©版权所有</p>' +
				'</div>'
		});
	},
	/**
	 *	YANGLI
	 * 	设置右键菜单
	 * 	2020-10-20 11:30:13
	 **/
	setContextMenu: function(jq_dom, menu) {
		if (typeof(jq_dom) === 'string') {
			jq_dom = $(jq_dom);
		}
		jq_dom.unbind('contextmenu');
		jq_dom.on('contextmenu', function(e) {
			if (menu) {
				Desktop._renderContextMenu(e.clientX, e.clientY, menu, this);
				if (e.cancelable) {
					// 判断默认行为是否已经被禁用
					if (!e.defaultPrevented) {
						e.preventDefault();
					}
				}
				e.stopPropagation();
			}
		});
	},
	/**
	 * 	YANGLI
	 * 	隐藏窗口
	 * 	2020-10-20 13:58:42
	 * */
	hideWins: function() {
		$('#task_bar_middle>.btn.show').each(function() {
			var index = $(this).attr('index');
			var layero = Desktop.getLayeroByIndex(index);
			$(this).removeClass('show');
			$(this).removeClass('active');
			layero.hide();
		})
	},
	/**
	 * 	YANGLI
	 * 	显示窗口
	 * 	2020-10-20 13:58:42
	 * */
	showWins: function() {
		$('#task_bar_middle>.btn').each(function() {
			var index = $(this).attr('index');
			var layero = Desktop.getLayeroByIndex(index);
			$(this).addClass('show');
			layero.show();
		});
		Desktop._checkTop();
	},
	getDesktopScene: function() {
		return $("#desktop-desktop-scene");
	},
	onReady: function(handle) {
		Desktop._handleReady.push(handle);
	}
};


$(function() {
	Desktop._init();
	for (var i in Desktop._handleReady) {
		var handle = Desktop._handleReady[i];
		handle();
	}
});
